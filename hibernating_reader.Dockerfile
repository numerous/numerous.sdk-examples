FROM python:3.9

RUN pip install numerous.sdk==0.39.2

COPY hibernating_reader.py job.py

ENTRYPOINT [ "python", "job.py" ]