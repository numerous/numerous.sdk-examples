FROM python:3.9

RUN pip install numerous.sdk
COPY numerous numerous
COPY hibernating_reader.py job.py

ENTRYPOINT [ "python", "job.py" ]