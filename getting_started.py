from datetime import timedelta
from math import pi, sin

from numerous.sdk import JobClient, JobStatus

with JobClient.connect() as job:
    job.status = JobStatus.RUNNING
    job.message = "Sine waving"

    for t in job.job_time.range(step=timedelta(minutes=1)):
        elapsed = job.job_time.start_to_time(t).total_seconds()
        if job.job_time.duration:  # only updates progress if the job has an end time
            job.progress = elapsed / job.job_time.duration.total_seconds()

        oscillations_per_second = 1 / (60 * 60)
        one_sine_wave_every_hour = sin(2 * pi * oscillations_per_second * elapsed)
        job.writer.row(t, {"sine": one_sine_wave_every_hour})

    job.status = JobStatus.FINISHED
    job.message = "Done waving"
