import logging
from datetime import datetime
from time import sleep

from numerous.sdk import JobClient

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("Reader Job")


def set_progress(job_client: JobClient, t: datetime) -> None:
    if job_client.job_time.duration is None:
        return

    progress_seconds = (t - job_client.job_time.start).total_seconds()
    duration_seconds = job_client.job_time.duration.total_seconds()
    job_client.progress = 100.0 * (progress_seconds / duration_seconds)


def main(job_client: JobClient) -> None:
    log.info("Starting hibernating reader")
    t = None
    job_client.set_hibernate_callback(
        lambda: log.info("Hibernating now, at time t=%s", t)
    )
    for t, data in job_client.read_inputs(step=20.0):
        log.info("Reading at t=%s", t)
        set_progress(job_client, t)
        sleep(0.05)
        seconds = (t - job_client.job_time.start).total_seconds()
        job_client.writer.row(
            seconds,
            {
                "passthrough_1": data["reader.variable_1"],
                "passthrough_2": data["reader.variable_2"],
                "passthrough_3": data["reader.variable_3"],
                "combination": (
                    data["reader.variable_1"]
                    + data["reader.variable_2"]
                    + data["reader.variable_3"]
                ),
            },
        )


if __name__ == "__main__":
    with JobClient.connect() as job_client:
        main(job_client)
