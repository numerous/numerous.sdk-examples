import json
from datetime import datetime, timedelta

from numerous.sdk import JobClient


class MySerializableObject:
    def __init__(self, my_value: float, my_time: datetime) -> None:
        self.my_value = my_value
        self.my_time = my_time

    def serialize(self) -> str:
        return json.dumps(
            {
                "type": "MySerializableObject",
                "my_value": self.my_value,
                "my_time": self.my_time.isoformat(),
            }
        )

    @staticmethod
    def deserialize(serialized: str) -> "MySerializableObject":
        data = json.loads(serialized)
        if data["type"] != "MySerializableObject":
            # This check is just to ensure that we did in fact serialize
            # this data as this kind of object
            raise TypeError(f"Got an unexpected object type {data['type']}")

        return MySerializableObject(
            my_value=data["my_value"],
            my_time=datetime.fromisoformat(data["my_time"]),
        )


with JobClient.connect() as job:
    my_serialized_object = job.state.get("my_stored_serialized_object")
    if my_serialized_object:
        # If some serialized object was saved
        my_deserialized_object = MySerializableObject.deserialize(my_serialized_object)
    else:
        # else, we create a new object
        my_deserialized_object = MySerializableObject(
            my_value=5.0,
            my_time=datetime.now(),
        )

    # And we modify it
    my_deserialized_object.my_value += 20.0
    my_deserialized_object.my_time += timedelta(days=5)

    # And finally serialize and save it in the state again
    job.state.set("my_stored_serialized_object", my_deserialized_object.serialize())
