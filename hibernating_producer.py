import logging
import math
import time
from dataclasses import dataclass
from datetime import datetime, timedelta
from typing import Optional

from numerous.sdk import JobClient
from numerous.sdk.connect import JobStatus
from numerous.sdk.models.component import Component

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("Job")

EXTRA_DATA = {f"x_tag_{i}": 999.0 for i in range(500)}


@dataclass
class SineWave:
    tag: str
    phase_shift: float
    vertical_shift: float
    period: float
    amplitude: float

    @staticmethod
    def from_component(component: Component) -> "SineWave":
        return SineWave(
            tag=component.parameters["tag"].value,
            phase_shift=component.parameters["phase_shift"].value,
            vertical_shift=component.parameters["vertical_shift"].value,
            period=component.parameters["period"].value,
            amplitude=component.parameters["amplitude"].value,
        )

    def get(self, t: datetime) -> float:
        b = 2.0 * math.pi / self.period
        return (
            self.amplitude * math.sin(b * (t.timestamp() + self.phase_shift))
            + self.vertical_shift
        )


@dataclass
class HibernationConfig:
    job_client: JobClient
    first_hibernate_time: datetime
    second_hibernate_time: datetime
    last_hibernate_time: Optional[datetime] = None

    def try_hibernate_now(self, t: datetime):
        hibernates_done = job_client.state.get("hibernates_done", 0)
        should_do_first_hibernation = (
            t >= self.first_hibernate_time and hibernates_done < 1
        )
        should_do_second_hibernation = (
            t >= self.second_hibernate_time and hibernates_done < 2
        )
        if not (should_do_first_hibernation or should_do_second_hibernation):
            return
        log.info("Hibernating now, at t=%s", t)
        job_client.state.set("hibernates_done", hibernates_done + 1)
        job_client.hibernate()

    @staticmethod
    def from_job_client(job_client: JobClient) -> "HibernationConfig":
        first_hibernate_time = job_client.job.parameters["first_hibernate_time"].value
        second_hibernate_time = job_client.job.parameters["second_hibernate_time"].value
        return HibernationConfig(
            job_client,
            job_client.job.time.start + timedelta(seconds=first_hibernate_time),
            job_client.job.time.start + timedelta(seconds=second_hibernate_time),
        )


def set_progress(job_client: JobClient, t: datetime) -> None:
    if job_client.job_time.duration is None:
        return

    progress_seconds = (t - job_client.job_time.start).total_seconds()
    duration_seconds = job_client.job_time.duration.total_seconds()
    job_client.progress = 100.0 * (progress_seconds / duration_seconds)


def main(job_client: JobClient):
    log.info("Loading job configuration")
    job_client.status = JobStatus.RUNNING
    components = job_client.scenario.components["sine_waves"].components["sine_wave"]
    sine_waves = [SineWave.from_component(comp) for comp in components]
    hibernation_config = HibernationConfig.from_job_client(job_client)

    log.info("Starting sine waving")
    for t in job_client.job_time.range(step=1):
        data = {sine_wave.tag: sine_wave.get(t) for sine_wave in sine_waves}
        log.info("Sine waving at t=%s data=%s", t, data)
        set_progress(job_client, t)
        hibernation_config.try_hibernate_now(t)
        job_client.writer.row(t, data | EXTRA_DATA)
        time.sleep(0.01)


if __name__ == "__main__":
    with JobClient.connect() as job_client:
        main(job_client)
