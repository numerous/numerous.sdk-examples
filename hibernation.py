from datetime import timedelta
from numerous.sdk import JobClient


with JobClient.connect() as job:

    def my_hibernation_callback():
        hibernate_count = job.state.get("hibernate_count", 0)
        hibernate_count += 1
        job.state.set("hibernate_count", hibernate_count)

    # Configure the hibernation callback
    job.set_hibernate_callback(my_hibernation_callback)

    # Hibernate the job
    job.hibernate()



with JobClient.connect() as job:
    next_write_time = job.state.get("next_write_time", job.job_time.start)
    for t in job.job_time.range(step=timedelta(hours=1), start=next_write_time)
        job.state.set("next_write_time", t)
        job.writer.row(t, {"value": 123.0})
